package br.ucsal.bes20192.testequalidade.escola.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractDAO {

	private static final String USER = "postgres";
	private static final String PASSWORD = "abcd1234";
	private static final String STRING_CONNECTION = "jdbc:postgresql://localhost:5432/escola";

	private static Connection connection = null;

	protected static Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection(STRING_CONNECTION, USER, PASSWORD);
		}
		return connection;
	}

}
